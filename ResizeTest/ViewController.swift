//
//  ViewController.swift
//  ResizeTest
//
//  Created by Daniel Lima on 20/09/17.
//  Copyright © 2017 Daniel Lima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var heightFirstView: NSLayoutConstraint!
    @IBOutlet var heightSecondView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toggleFirstView(_ sender: Any) {
        
        if(heightFirstView.constant == 82){
            
            heightFirstView.constant = 298
            UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
            }
            
        }else{
            
            heightFirstView.constant = 82
            UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
            }
            
        }
        
    }
    
    @IBAction func toggleSecongView(_ sender: Any) {
        
        if(heightSecondView.constant == 82){
            
            heightSecondView.constant = 298
            UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
            }
            
        }else{
            
            heightSecondView.constant = 82
            UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
            }
            
        }
        
    }
    
    
}

